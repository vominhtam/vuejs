import {API_URL} from "./config";


export default {
  server: {
    port: process.env.PORT_SERVER || 80,
    host: '0.0.0.0'
  },
  mode: 'universal',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.NPM_PACKAGE_NAME || '',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.NPM_PACKAGE_DESCRIPTION },
      { property: 'og:type', content: 'website'},
      { property: 'og:locale', content: 'vi_VN'},
      { property: 'og:site_name', content: process.env.NPM_PACKAGE_DESCRIPTION}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {  href : "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" ,rel :"stylesheet" },
    ],
    script: [
      {src: '/js/jquery-3.1.1.min.js'},
      {src: '/js/popper.min.js'},
      {src: '/js/bootstrap.min.js'},
      {src: '/js/slick.js'},
      {src: '/js/owl.carousel.js'},
      {src: '/js/modal.js'},
      {src: '/js/select2.min.js'},
      {src: '/js/page_all.js'},
      {src: 'https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js'},
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/bootstrap.min.css',
    '~/assets/css/select2.min.css',
    '~/assets/css/owl.carousel.css',
    '~/assets/css/owl.theme.default.css',
    '~/assets/css/style.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-validate',
    { src : '~/plugins/vue-count-down', mode: 'client' },
    { src : '~/plugins/vue-loader', mode: 'client' },
    { src : '~/plugins/vue-postcode', mode: 'client' },
    { src : '~/plugins/vue-kakao', mode: 'client' },
    // { src: '~/plugins/star-rating.js', mode: 'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [

  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'cookie-universal-nuxt',
  ],

  axios: {
    baseURL: API_URL,
    debug: process.env.DEBUG || false,
    proxyHeaders: false,
    credentials: false,
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'account/login', method: 'post', propertyName: 'token' },
          user: { url: 'account/me', method: 'get' },
        },
        user: {
          property: 'data',
          // autoFetch: true
        },
        redirect: {
          login: '/login',
          logout: '/',
          callback: '/login',
          home: '/'
        },
        tokenRequired: true,
        tokenType: 'bearer',
        globalToken: true,
        autoFetchUser: true,
        logout: false
      }
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {

  }
}
