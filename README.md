# letuins-fe-vuejs

## Build Setup

```bash
# install dependencies
$ npm install

#Copy config api base
$ cp config.js.example config.js

#Copy .env file
cp .env.example .env

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
# Need: npm install -g pm2
# Config vhosts with proxy port config in .env file
$ cp ecosystem.config.js.example ecosystem.config.js
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
