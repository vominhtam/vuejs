$(document).ready(function () {
  let years = new Date().getFullYear()

  let timers = []

  for (let i = 1950 ; i <= years ; i++) {
    timers.push({
      text : `${i}년`
    })
  }

  let mouths =  [
    {
      text: '1월',
      id: 1
    }, {
      text: '2월',
      id: 2
    },
    {
      text: '3월',
      id: 3
    },
    {
      text: '4월',
      id: 4
    }, {
      text: '5월',
      id: 5
    }, {
      text: '6월',
      id: 6
    }, {
      text: '7월',
      id: 7
    },
    {
      text: '8월',
      id: 8
    },
    {
      text: '9월',
      id: 9
    },
    {
      text: '10월',
      id: 10
    },
    {
      text: '11월',
      id: 11
    },
    {
      text: '12월',
      id: 12
    }
  ]
  $('#time1').click(function () {
    $(this).selectScroll({
      data: [
        timers,
        mouths
      ],
      title: '졸업년월',
      selectedIndex: [40, 5, 0],
      class: "show",
    })
    let val = $(this).val();
    if (val) {
      let year = val.split('-')[0];
      let month = val.split('-')[1];
      let inYear = parseInt(year.split('년')[0]);
      let inMonth = parseInt(month.split('월')[0]);
      let topYear = 0 -((inYear - 1950) * 31.25);
      let topMonth = 0 -((inMonth -1 ) * 31.25);
      let picker = $(".picker.show").find('.picker-content').find('.wheel-wrapper').find('.wheel');
      picker.each(function (e, item) {
        if (e ===0 ){
          $(item).find('.wheel-scroll').css({'transform' : 'translate(' + 0 +', ' + topYear + 'px) scale(1) translateZ(0px)'});

        }
        if (e === 1) {
          $(item).find('.wheel-scroll').css({'transform' : 'translate(' + 0 +', ' + topMonth + 'px) scale(1) translateZ(0px)'});
        }
      })
    }
  });
  $('#time2').click(function () {
    $(this).selectScroll({});
    $('.picker').addClass('show');
    let val = $(this).val();
    if (val) {
      let year = val.split('-')[0];
      let month = val.split('-')[1];
      let inYear = parseInt(year.split('년')[0]);
      let inMonth = parseInt(month.split('월')[0]);
      let topYear = (inYear - 1950) * 30;
      let topMonth = 0 -((inMonth -1 ) * 30);
      let picker = $(".picker.show").find('.picker-content').find('.wheel-wrapper').find('.wheel');
      picker.each(function (e, item) {
        if (e ===0 ){
          $(item).find('.wheel-scroll').css({'transform' : 'translate(' + 0 +', ' + topYear + ')'});
        }
        if (e === 1) {
          $(item).find('.wheel-scroll').css({'transform' : 'translate(' + 0 +', ' + topMonth + ')'});
        }
      })
    }
  });

  $('#time2').selectScroll({
    data: [
      timers,
      mouths
    ],
    title: '입학년월',
    selectedIndex: [40, 5, 0],
  });

});
