$(document).ready(function () {

  // active navbar of page current
  var urlcurrent = window.location.href;
  $(".navbar-menu li a[href$='"+urlcurrent+"']").addClass('active');

  //call slick js
  $('.banner').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerPadding: 0,
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
  });
  var owlBookList = $('.textbook__list');
  owlBookList.owlCarousel({
    items: 1
  });
// Go to the next item
  $('.textbook__next').click(function() {
    owlBookList.trigger('next.owl.carousel');
  });
// Go to the previous item
  $('.textbook__prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owlBookList.trigger('prev.owl.carousel', [300]);
  });
  var owlTeacherList = $('.teacher__list');
  owlTeacherList.owlCarousel({
    items: 2
  });
// Go to the next item
  $('.teacher__next').click(function() {
    owlTeacherList.trigger('next.owl.carousel');
  });
// Go to the previous item
  $('.teacher__prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owlTeacherList.trigger('prev.owl.carousel', [300]);
  });

  var owlEventList = $('.event__list');
  owlEventList.owlCarousel({
    items: 1
  });
// Go to the next item
  $('.event__next').click(function() {
    owlEventList.trigger('next.owl.carousel');
  });
// Go to the previous item
  $('.event__prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owlEventList.trigger('prev.owl.carousel', [300]);
  });

});
