$(document).ready(function () {

  // active navbar of page current
  var urlcurrent = window.location.href;
  $(".navbar-menu li a[href$='"+urlcurrent+"']").addClass('active');

  //call slick js
  $('.banner').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerPadding: 0,
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
  });
  $('.textbook__list').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    centerPadding: 0,
    prevArrow: $('.textbook__prev'),
    nextArrow: $('.textbook__next'),
  });
  $('.teacher__list').slick({
    centerMode: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    fade: false,
    centerPadding: 0,
    prevArrow: $('.teacher__prev'),
    nextArrow: $('.teacher__next'),
  });
  $('.event__list').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    centerPadding: 0,
    dots: true,
    appendDots: $('.event_dots'),
    prevArrow: $('.event_prev'),
    nextArrow: $('.event_next'),
  });

});
