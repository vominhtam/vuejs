$(document).ready(function () {
  let years = new Date().getFullYear()

  let timers = []

  for (let i = 1950 ; i <= years ; i++) {
    timers.push({
      text : `${i}년`
    })
  }

  let mouths =  [
    {
      text: '1월',
      id: 1
    }, {
      text: '2월',
      id: 2
    },
    {
      text: '3월',
      id: 3
    },
    {
      text: '4월',
      id: 4
    }, {
      text: '5월',
      id: 5
    }, {
      text: '6월',
      id: 6
    }, {
      text: '7월',
      id: 7
    },
    {
      text: '8월',
      id: 8
    },
    {
      text: '9월',
      id: 9
    },
    {
      text: '10월',
      id: 10
    },
    {
      text: '11월',
      id: 11
    },
    {
      text: '12월',
      id: 12
    }
  ]
  $('#time1').click(function () {
    $(this).selectScroll({
      data: [
        timers,
        mouths
      ],
      title: '입학년월',
      selectedIndex: [40, 5, 0],
      class: "show",
    })
  });
  $('#time2').click(function () {
    $(this).selectScroll({});
    $('.picker').addClass('show');
  });

    $('#time2').selectScroll({
      data: [
        timers,
        mouths
      ],
      title: '졸업년월',
      selectedIndex: [40, 5, 0],
    });
  addTime()
});

function addTime() {
  let yearOption  = ''
  let years = new Date().getFullYear() - 14

  for (let i = years ; i >= 1900 ; i-- ) {
    yearOption += `<option value="${i}">${i}년</option>`
  }
  $('#year').append(yearOption)

  let dayOption = ''
  for (let i = 1 ; i < 32 ; i++) {
    let day = i < 10 ? `0${i}` : i
    dayOption += `<option value="${day}">${day}일</option>`
  }
  $('#day').append(dayOption)
}
