$(document).ready(function () {
  //call slick js
  $('.banner').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerPadding: 0,
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
  });
  $('.intro__list').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerPadding: 0,
    dots: true,
    arrows: false
  });

  $('.event__list').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    centerPadding: 0,
    dots: true,
    appendDots: $('.event_dots'),
    prevArrow: $('.event_prev'),
    nextArrow: $('.event_next'),
  });



});
