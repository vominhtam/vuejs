$(document).on('click','.valueChecked', function () {
    let itemCheckAll = $(".checkAll");
    let componentItem = $(".valueChecked")
    if ($(this).is(':checked')) {
      $(this).parent().removeClass('not_checked').addClass('checked');
      if (!itemCheckAll.is(":checked") && componentItem.length === $('.valueChecked:checkbox:checked').length) {
        itemCheckAll.prop('checked', true);
      }
    } else {
      $(this).parent().removeClass('checked').addClass('not_checked');
      if (itemCheckAll.is(":checked")) {
        itemCheckAll.prop('checked', false);
      }
    }

    if ($('.valueChecked:checkbox:checked').length) {
      $("#btn_addFreePass").css({'background-color' : '#244cba'})
      $("#btn_addFreePass a").css({'color' : 'white'})
      $("#btn_addFreePass span").css({'color' : 'white'})
    } else {
      $("#btn_addFreePass").css({'background-color' : '#f7f7f7'})
      $("#btn_addFreePass a").css({'color' : 'black'})
      $("#btn_addFreePass span").css({'color' : 'black'})
    }


    $("#number_freepass").html(`(${$('.valueChecked:checkbox:checked').length})`);
});

$(document).on('click','.checkAll', function () {
  if ($(this).is(':checked')) {
    $(".valueChecked").prop('checked', true).parent().removeClass('not_checked').addClass('checked')
    $("#btn_addFreePass").css({'background-color' : '#244cba'})
    $("#btn_addFreePass a").css({'color' : 'white'})
    $("#btn_addFreePass span").css({'color' : 'white'})
  } else {
    $('.valueChecked').prop('checked', false).parent().removeClass('checked').addClass('not_checked');
    $("#btn_addFreePass").css({'background-color' : '#f7f7f7'})
    $("#btn_addFreePass a").css({'color' : 'black'})
    $("#btn_addFreePass span").css({'color' : 'black'})
  }
  $("#number_freepass").html(`(${$('.valueChecked:checkbox:checked').length})`);
});
