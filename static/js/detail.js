$(document).ready(function () {
  $('.box-select .selected').on('click', function () {
    $(this).parent().toggleClass('open');
  });
  $('.select_list li').on('click', function () {
    let times = $(this).data('time');
    let days = $(this).data('day');
    let selected = `<div class="flex-lc">${days} ${times}</div>`
    $(this).parents('.box-select').find('.selected').html(selected);
    $('.box-select').removeClass('open');
  });

  $(document).mouseup(function (e) {
    var form_group = $('.box-select');
    if (!form_group.is(e.target) && form_group.has(e.target).length === 0) {
      $('.box-select').removeClass('open');
    }
  });
});
