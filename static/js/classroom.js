$(document).ready(function () {
  $('#modal-filter').modal('show');
  $('#modal-filter-teacher').modal('show');
  $('.btn_td').on('click',function () {
    $(this).parents('.item').toggleClass('active');
  });

  $('.modal-filter .checkbox_lv1').on('change load', function () {
    if ($(this).is(':checked')) {
      $(this).parents('li').find('.filter__sub input').prop("checked", true);
    } else {
      $(this).parents('li').find('.filter__sub input').prop("checked", false);
    }
  });
  $('.filter__sub input[type="checkbox"]').on('change load', function () {
    if ($(this).is(':checked')) {
      $(this).parents('li').find('.checkbox_lv1').prop("checked", false);
    } else {
      $(this).parents('li').find('.checkbox_lv1').prop("checked", false);
    }
  });

  $('.modal-filter li').click(function () {
    $(this).addClass('active');
  });
});
