$(document).ready(function () {
  //call select2.min.js
  $('.select-2').select2();

  //call slick js
  $('.banner').slick({
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    centerPadding: 0,
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
  });
});
