import axios from 'axios'
import {API_URL, OPEN_API} from "../config";
var qs = require('qs');

export const ApiService = {

  setFormData() {
    axios.defaults.headers.common[
      "Content-Type"
      ] = "multipart/form-data";
  },

  setHeader() {
    if (process.browser) {
      axios.defaults.headers.common["Authorization"] = localStorage.getItem('auth._token.local');
    }
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`ApiService ${error}`)
    })
  },

  get(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return axios.get(API_URL + url, {
      params: params,
      paramsSerializer: params => {
        return qs.stringify(params)
      }})
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        return error.response.data
      })
  },

  post(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return axios.post(API_URL + url, params)
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        return error.response.data
      })
  },

  put(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return axios.put(API_URL + url, params)
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        return error.response.data
      })
  },


  delete(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return axios.delete(API_URL + url, {params: params})
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        return error.response.data
      })
  },

  async getRequest(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return await axios.get(API_URL + url, {params: params})
      .then((res) => {
        return res.data.data
      })
      .catch((error) => {
        return error.response.data
      })
  },

  async postRequest(url, params = {}, isToken = false) {
    if (isToken) {
      this.setHeader();
    }
    return await axios.post(API_URL + url, params)
      .then((res) => {
        return res.data.data
      })
      .catch((error) => {
        return error.response.data
      })
  },
}

const BANNER = 'banner'
export const BannerApiService = {
  getBannerByCategory(category, lec_category = null) {
    let params = {'category' : category}
    if (lec_category) {
      params.lec_category = lec_category
    }
    return ApiService.get(BANNER, params);
  },
  getCalendar() {
    return ApiService.get(BANNER + '/calendar')
  }
}

const NOTIFICATION = 'write_notifications'
export const NotificationApiService = {
  getNotifications(params) {
    return ApiService.get(NOTIFICATION, params);
  },
}

const ACCOUNT = 'account/'
export const AuthService = {
  getOTP(params) {
    return ApiService.post(ACCOUNT + 'check-phone', params)
  },
  checkId(params) {
    return ApiService.post(ACCOUNT + 'confirm-id', params)
  },
  finID(params) {
    return ApiService.post(ACCOUNT + 'find_id', params)
  },
  checkEmail(params) {
    return ApiService.post(ACCOUNT + 'check-email', params)
  },
  getValSchool(params) {
    return axios.get( OPEN_API, {
      params: params,
      withCredentials: false,
      transformRequest: (data, headers) => {
        delete headers.common['Authorization'];
        return data;
      }
    })
      .then((res) => {
        return res.data.dataSearch
      })
      .catch((error) => {
        return error.response.data
      })
  },

  register(params) {
    return ApiService.post(ACCOUNT + 'register' , params)
  },
  addInfo(params) {
    return ApiService.post(ACCOUNT + 'add-information', params)
  },
  findPassword(params) {
    return ApiService.post(ACCOUNT + 'find_password', params)
  },
  changePassword(id, params) {
    return ApiService.post(ACCOUNT + 'change_password/'+ `${id}`, params)
  },
  checkIdLogin(params) {
    return ApiService.post(ACCOUNT + 'check_id', params)
  },
  me() {
    return ApiService.get(`${ACCOUNT}me`, {}, true)
  },
  update(params) {
    return ApiService.put(`${ACCOUNT}me`, params, true)
  },
  checkNick(params) {
    return ApiService.post(`${ACCOUNT}check-nick`, params)
  },
  login(params){
    return ApiService.post(`${ACCOUNT}login`, params)
  },
  withDraw(params) {
    return ApiService.post(`${ACCOUNT}me/withdraw`, params, true);
  }
}

const TEACHER = 'teacher/'

export const TeacherApiService = {
  getTeacherOnline() {
    return ApiService.get(TEACHER + 'is-online')
  },
  getTeacherOffline() {
    return ApiService.get(TEACHER + 'is-offline', {
      page: 1,
      limit: 10
    })
  },
  getTeacherByLecture(uid) {
    return ApiService.get(TEACHER + 'lecture/' + uid)
  }
}
const LECTURE = 'lecture/'

export const LectureOnlineService = {
  getOnline(lec_category) {
    let params = {'lec_category' : lec_category}
    return ApiService.get(LECTURE + 'online', params)
  },
  getMyOnline(params) {
    return ApiService.get(LECTURE + 'online/get_lectures', params)
  },
  getSelfIntro(params) {
    return ApiService.get(LECTURE + 'self_intro/get_lectures', params)
  },
  getRecommendLecture(params) {
    return ApiService.get(LECTURE + 'recommend', params)
  },
  getLectureMovieInfo(params){
    return ApiService.get(LECTURE + params.lecture_no + '/get_lecture_movie_info', params)
  },
  getMovieClassRoom(params){
    return ApiService.get(LECTURE + params.lecture_no + '/get_class_room', params)
  },
  getPackageMovieInfo(params){
    return ApiService.get(LECTURE + params.uid + '/get_package_movie_info', params)
  },
  getDetailLecture(params) {
    return ApiService.get(`${LECTURE}single/detail`, params)
  },
  getListOnlineLecture(params) {
    return ApiService.get(`${LECTURE}online/get_lecture_list`, params)
  },
  getListOfflineLecture(params) {
    return ApiService.get(`${LECTURE}offline/get_single_and_package_lecture_list`, params)
  },
  getParamOnlineLecture() {
    return ApiService.get(`${LECTURE}online/get_param_online`)
  },

  getParamOfflineLecture() {
    return ApiService.get(`${LECTURE}online/get_param_offline`)
  },

  getParamsPublicLecture() {
    return ApiService.get(`${LECTURE}public/get_category_init`)
  },
  getDetailCompanyBook(params) {
    return ApiService.get(`${LECTURE}company_book/detail`, params)
  },
  movie(params) {
    return ApiService.get(`${LECTURE}movie/movie_player`, params);
  },
  kollusVideo(params) {
    return ApiService.get(`${LECTURE}movie/movie_link`, params);
  },
  getListPublicLecture(params) {
    return ApiService.get(`${LECTURE}public/get_lecture_list`, params)
  },
}


const CONFIG = 'config'
export const ConfigApiService = {
  get() {
    return ApiService.get(CONFIG)
  }
}


const BOOK = 'book/'
export const BookApiService = {
  getBookOnline() {
    return ApiService.get(BOOK + 'online')
  },
  getListBookPublic() {
    return ApiService.get(BOOK + 'public');
  },
  getListSaleBook(params) {
    return ApiService.get(`${BOOK}get_book_sale_list`, params);
  },
}

const MYBOOK = 'mybook/'
export const MyBookServiceService = {
  getTransportBook(params = {}) {
    return ApiService.get(MYBOOK + params.user_id + '/get_book_my_list', params);
  }
}

const MYCLASS = 'lecture/';

export const MyClassApiService = {
  getLectureEmploymentComAnalysisMyList(params = {}) {
    return ApiService.get(MYCLASS + 'employment_company_analysis/get_lectures', params);
  },
  getLectureAcademy(params = {}){
    return ApiService.get(MYCLASS + 'academy/get_lectures', params)
  },
  getProcessTest(params = {}){
    return ApiService.get(MYCLASS + 'practice/get_lectures', params)
  },
  getLectureEnded(params = {}){
    return ApiService.get(MYCLASS + 'close_content/get_lectures', params)
  },
  getListLecturePractice(params = {}) {
    return ApiService.get(MYCLASS + 'test/get_lectures', params)
  },
  getDataTestReport(params = {}){
    return ApiService.get('test/get_test_report', params)
  },
  getFreePass(params = {}) {
    return ApiService.get(MYCLASS + 'freepass/get_lectures', params)
  },
  getListLectureStudy(params = {}) {
    return ApiService.get(MYCLASS + 'lms_study/get_lectures', params);
  },
  getSubListLecture(id, params = {}) {
    return ApiService.get(MYCLASS + `freepass/${id}/get_sub_list`, params)
  },
  getAddListLecture(id, params) {
    return ApiService.get(MYCLASS + `freepass/${id}/get_add_list`, params)
  },
  addFreePass(params) {
    return ApiService.post(MYCLASS + `freepass/add_lecture`, params)
  },
  removeFreePass(params) {
    return ApiService.put(MYCLASS + `freepass/remove_lecture`, params)
  },
  favorite(params) {
    return ApiService.post(MYCLASS + `favorite`, params)
  },
  packages(params) {
    return ApiService.get(`${MYCLASS}package/detail`, params)
  },
  packageLecture(params) {
    return ApiService.get(`${MYCLASS}package/get_lectures`, params)
  },
  getDetailClass(params){
    return ApiService.get(MYCLASS + 'lms_study/get_mystudy_list', params)
  },
  getDetailRecommendLecture(params) {
    return ApiService.get(MYCLASS + 'detail/recommendation_lecture', params)
  },
  getCourseDetail(params){
    return ApiService.get( MYCLASS + '/lms_study/get_course_detail', params)
  },
  getPopupCetificate(params){
    return ApiService.get( 'popup/get_study_cert_popup', params)
  }
}

const COUPON = 'coupon/'
export const CouponApiService = {
  get(id, params) {
    return ApiService.get(COUPON + `${id}/get_coupons` , params)
  },
  register(id, params) {
    return ApiService.post(COUPON + `${id}/register_coupons` , params)
  },
  getOrder(id, params) {
    return ApiService.get(COUPON + `${id}/get_order_coupon` , params)
  }
}


const SEARCH = 'search/default'
export const SearchApiService = {
  get(params) {
      return ApiService.get(SEARCH , params)
  },
  getHistoryKeyword(params) {
    return ApiService.get('search/get_keyword_history', params)
  },
  getTop5() {
    return ApiService.get('search/get_top5_keyword_history')
  },
  clearAllKeyword(params) {
    return ApiService.post('search/clear_keyword_history', params);
  }
}

const POINT = 'point/'
export const PointApiService = {
  get(params) {
    return ApiService.get(`${POINT}get_my_point_list` , params)
  },
  convert(params) {
    return ApiService.post(`${POINT}change_money` , params)
  },
  getMoney(params) {
    return ApiService.get(`${POINT}get_my_money_list` , params)
  }
}


const TRANSACTION = 'transaction/'
export const TransactionApiService = {
  get(id, params) {
    return ApiService.get(`${TRANSACTION}${id}/get_my_orders`, params)
  },
}
const REVIEW = 'review/';

export const ReviewApiService = {
  createReview(params) {
    return ApiService.post(REVIEW + 'create_review', params);
  }

}

const OLDTEXT = 'old-text/'
export const OldTextApiService = {
  getPublic(date) {
    return ApiService.get(OLDTEXT + 'public', {
      date: date
    })
  }
}

const cart = 'cart/'
export const CartApiService = {
  getCartUser(params = {}) {
    return ApiService.get(cart + 'list', params, true)
  },
  count() {
    return ApiService.get(cart + 'count', {}, true)
  },
  delete(param) {
    return ApiService.delete(cart + 'delete', param, true)
  },
  addToCart(param) {
    return ApiService.post(cart + 'add-ajax', param, true)
  }
}

const NOTICE = 'qa/'
export const QaApiService = {
  get(params) {
    return ApiService.get(`${NOTICE}get_ask_admin_list_and_comment`, params);
  },
  create(params) {
    ApiService.setFormData();
    return ApiService.post(`${NOTICE}create_ask_admin_question`, params);
  },
  delete(params){
    return ApiService.delete(`${NOTICE}delete_ask_admin_question`, params);
  },
  update(params) {
    ApiService.setFormData();
    return ApiService.put(`${NOTICE}update_ask_admin_question`, params);
  },
  delete_image(params){
    return ApiService.delete(`${NOTICE}delete/ask_admin_file`, params);
  },
}

const CATEGORY = 'category/'
export const CategoryApiService = {
  page(params) {
    return ApiService.get(`${CATEGORY}get_mypage_category_summary`, params);
  },
  class(params) {
    return ApiService.get(`${CATEGORY}get_class_category_summary`, params);
  },
}

const FREE = 'free/'
export const FreeLectureApiService = {
  get(params) {
    return ApiService.get(`${FREE}public`, params);
  },
}


const POPUP = 'popup/'
export const GuideBookApiService = {
  get(params) {
    return ApiService.get(`${POPUP}get_guide_book_pop`, params);
  },
  getRefundStatus(params){
    return ApiService.get(`${POPUP}get_gong_refund_status`, params);
  },
  getStudyGoal(params) {
    return ApiService.get(`${POPUP}get_study_goal_popup`, params)
  },
  getStudyGoalSet(params) {
    return ApiService.get(`${POPUP}get_study_goal_set`, params)
  },
  setStudyGoalSet(params) {
    return ApiService.post(`${POPUP}start_study_tutorial_popup`, params)
  },
  getRefundStatus2(params) {
    return ApiService.get(`${POPUP}get_mypage_refund_status`, params)
  },
  getTrackingTutorialPopup(params) {
    return ApiService.get(`${POPUP}get_tracking_tutorial_popup`, params)
  },
  setTrackingTutorialPopup(params) {
    return ApiService.post(`${POPUP}tracking_tutorial_popup`, params)
  }
}

const REFUND = 'refund/'
export const RefundApiService = {
  post(params) {
    return ApiService.post(`${REFUND}create_refund_pass`, params);
  },
  postPublic(params) {
    return ApiService.post(`${REFUND}create_refund_public_company`, params);
  },
  postAttendance(params) {
    return ApiService.post(`${REFUND}create_refund_attendance`, params);
  },
}

const TEXTBOOK = 'book/'
export const TextbookApiService = {
  getBookSaleList(params) {
    return ApiService.get(TEXTBOOK + 'get_book_sale_list', params);
  },
}

const ORDER = 'order'
export const OrderApiService = {
  order(params) {
    return ApiService.post(ORDER, params, true);
  },
  show(id) {
    return ApiService.get(ORDER + '/' + id, {}, true);
  },

  updateOrder(params) {
    return ApiService.post(ORDER + '/update', params, true);
  },
}


export const ReviewDetailApiService = {
  single(params) {
    return ApiService.get('lecture/single/detail_reviews', params);
  },
  package(params) {
    return ApiService.get('lecture/package/detail_reviews', params);
  },
}

export const MovieDetailService = {
  single(params) {
    return ApiService.get('lecture/single/detail_movies', params);
  },

}


const ADDREM = 'addrem/'


export const AddremApiService = {
  reUpdate(uid, formData) {
    ApiService.setFormData();
    return ApiService.post(ADDREM + 're-update?mystudy_id=' + uid , formData);
  },
  upload(uid, formData) {
    ApiService.setFormData();
    return ApiService.post(ADDREM + 'upload?mystudy_id=' + uid , formData);
  },

  delete(uid) {
    return ApiService.delete(ADDREM + uid);
  },
}






