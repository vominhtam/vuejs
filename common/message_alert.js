import {getMsgValidate} from "./validation";

export function messageRegister() {
  return {
    phone : {
      required : "휴대폰 번호를 입력해 주세요.",
      length : "휴대폰 번호 11자리를 입력해 주세요.",

    }
  }
}

export function msgValidateRegister(v, field, value = null, valueMax = null) {

  for (let parameter in v.$params) {

    if (!v[parameter]) {
      alert(messageRegister(value, valueMax)[field][parameter] ?? '이 필드는 유효하지 않습니다\n')
      return true
    }
  }
  return false
}
