import moment from 'moment'
export function formatDashPhone(phone) {
  phone = phone.split('-').join('');
  if (phone.length <= 3) {
    return phone.slice(0,3)
  } else if (phone.length <= 7) {
    return phone.slice(0,3)+"-"+ phone.slice(3,7)
  } else {
    return phone.slice(0,3)+"-"+ phone.slice(3,7)+"-"+ phone.slice(7,15);
  }
}

export function formatID(id) {
  return id.slice(0,1) + id.slice(1, id.length - 1).replace(/./g, '*');
}

export function formatDate(date, format = 'YYYY.MM.DD') {
  return moment(String(date)).format(format)
}

export function formatDayOrMonthOrYear(date) {
  return String(date).length < 2 ? "0"+date : date;
}

export function diffDayNow(endDate) {

  let now = moment().startOf('day');
  let end = moment(endDate, "YYYY-MM-DD");
  return moment.duration(end.diff(now)).asDays();

}
export function onlyNumber(evt, except = null) {
  evt = (evt) ? evt : window.event;
  let charCode = (evt.which) ? evt.which : evt.keyCode;
  if (except && except === charCode) return true;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    evt.preventDefault()
  }
  return true;
}

export function preventSpecialCharacter(value) {
  let specials=/[$&+,:;=?@#|'<>.^*~`_/()%!-]/;
  return specials.test(value);
}

export function myDateFormat(date, format){
  return moment(String(date)).format(format)
}

export function parseLectureAmountInfo(lec_amount_info, lec_amount_info2, lec_amount_info3){
  let amount = ''
  if (lec_amount_info) {
    amount = lec_amount_info.split(',')
  }
  else if (lec_amount_info2) {
    amount = lec_amount_info2.split(',')
  }
  else if (lec_amount_info3) {
    amount = lec_amount_info3.split(',')
  }

  return {
    'price' : amount[1],
    'period' : amount[0],
    'discount' : amount[1] - amount[2],
    'percent' : ((amount[1] - amount[2]) / (amount[1] * 100))
  }
}

export const formatCurrency = (value) => {
  let floatValue = (value).toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  return floatValue.substring(0, floatValue.length - 2);
};

export const formatMoney = (value, currency = '', format = '.') => {
  let val = value.toString()
    .replace(/\D/g, '')
    .replace(/\B(?=(\d{3})+(?!\d))/g, format)

  return  (currency) ? val + currency : val;
};

export const intervalNumberFormat = (number) => {
  return  (number.replace(/[^0-9]+/g, ""));
};

export const subDays = (ago) => {
  let now = new Date();
  now.setMonth(now.getMonth() - ago);
  return  Math.abs((now.getTime() - new Date().getTime()) / (1000 * 3600 * 24));
};

export const subWeek = (date) => {
  let week = new Array('일', '월', '화', '수', '목', '금', '토');
  let day = new Date(date).getDay();

  return week[day];
};

export const subWeekKorean = (date) => {
  let dt = new Date(date)
  let week = new Array('일', '월', '화', '수', '목', '금', '토');
  dt.setHours(dt.getHours() - Math.abs(dt.getTimezoneOffset())/60);
  let day = dt.getDay();
  return week[day];
};

export const bankAccount = (account) => {
  return account.replace("X", "/ ");
}

export const sumProp = (items, prop) => {
  if (items == null || items == '') {
    return 0
  }
  return items.reduce(function (a, b) {
    return b[prop] == null ? a : a + b[prop]
  }, 0)
};

export const convertUrlImage = (contents,subString, replaceString) => {
  String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
  };
  let index = null;
  let start = 0;
  do {
    index = contents.indexOf(subString, start);
    if (index !== -1) {
      let next4Char = contents[index + subString.length]
        + contents[index + subString.length + 1]
        + contents[index + subString.length + 2]
        + contents[index + subString.length + 3]
      if (next4Char !== 'http') {
        contents =  contents.splice(index + subString.length, 0, replaceString)
      }
      start = index + subString.length;
    }
  }while (index !== -1);

  return contents;
}


export const preventCopyAndPaste = (e) => {
  var regex = /[^0-9]/gi;
  var key = e.clipboardData.getData('text')
  if (regex.test(key)) {
    e.preventDefault();
    return false;
  }
}

export const preventCopyAndPasteNick = (e) => {
  var regex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
  var key = e.clipboardData.getData('text')
  if (!regex.test(key)) {
    e.preventDefault();
    return false;
  }
}

export const redirectSingleOrPackage = (lecture) => {
    if (lecture.type == 'lec') {
      window.location.href = '/details/single/' +lecture.no
    } else {
      window.location.href = '/details/package/' +lecture.no
    }
}



