import {length} from "./helpers_validate";
import {alphaNum} from "vuelidate/lib/validators";

export function getMsgValidate(field = null, value = null, valueMax = null) {
  return {
    required: field + ' 입력하세요.',
    length : `${field} 번호 ${value}자리를 입력해 주세요`,
    sameAs: field + ' 정확하지 않습니다.',
    alphaNum : '이 필드는 구문 상 잘못되었습니다.',
    minLength : `${field} ${value} 자 이상이어야합니다.`,
    maxLength : `${field} ${valueMax} 자 미만이어야합니다.`
  }
}

export function msgValidateUpdate(v, field, value = null,  valueMax = null) {

  for (let parameter in v.$params) {

    if (!v[parameter]) {
      return getMsgValidate(field, value, valueMax)[parameter] ?? '이 필드는 유효하지 않습니다\n'
    }

  }
}

export function getMsgValidateLogin(value = null) {
  return {
    required: value + ' 입력하세요.',
    minLength: '영문, 숫자 공백 없이 6자 ~ 12자 이내로 입력해주세요.',
    maxLength: '영문, 숫자 공백 없이 6자 ~ 12자 이내로 입력해주세요.'
  }
}

export function msgValidate(field, value = null) {
  try {
    return getMsgValidateLogin(value)[field]
  } catch (e) {
    return "unknown"
  }
}
