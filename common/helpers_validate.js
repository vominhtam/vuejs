import { helpers } from 'vuelidate/lib/validators'
export const length = (param) => (value) => !helpers.req(value) || value.split('-').join('').length === param
export const checked = (value) => value === true

let regexEndEmail = /^((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/
export const endEmail = (value) => !helpers.req(value) || regexEndEmail.test(value)

let regexNick = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/
export const nick = (value) =>  regexNick.test(value)
