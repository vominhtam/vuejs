export const getMsgValidate = {
  name: {
    required: "이름을 입력하세요.",
    minLength: "이름을 2 자 이상이어야합니다.",
    maxLength: "이름을 6 자 미만이어야합니다.",
    alphaNum: ""
  },
  confirm: {
    required: "휴대전화 인증을 완료해주세요.",
    sameAs: "인증번호가 정확하지 않습니다."
  },
  phone: {
    required: "휴대폰 번호를 입력해 주세요.",
    length: "휴대폰 번호 11자리를 입력해 주세요."
  },
  id: {
    minLength: "아이디는 6자 이상, 12자 이하여야 합니다.",
    maxLength: "아이디는 6자 이상, 12자 이하여야 합니다.",
    alphaNum : "아이디는 영문, 숫자 조합하여 최대 12개까지 입력가능합니다.",
    required : "아이디를 입력하세요."
  },
  password: {
    required : "비밀번호를 입력하세요",
    minLength : "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. ",
    maxLength : "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. ",
    alphaNum : "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. "
  },
  confirmPassword: {
    required : "비밀번호 확인을 입력하세요",
    minLength: "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. ",
    maxLength: "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. ",
    alphaNum : "영문+숫자 조합, 공백 없이 6자~12자리 이내로 입력해주세요. ",
    sameAs: "비밀번호가 일치하지 않습니다."
  },
  firstEmail: {
    required : "이메일을 입력하세요.",
    maxLength: "이메일을 60 자 미만이어야합니다."
  },
  endEmail: {
    required : "이메일 도메인을 선택해 주세요.",
    endEmail : "잘못된 이메일 형식입니다."
  },
  univ: {
    required : "학교를 선택하세요. "
  },
  graduationDate: {
    required : "재학기간을 선택하세요."
  },
  admissionDate: {
    required : "재학기간을 선택하세요."
  },
  classification: {
    required : "학과를 선택하세요."
  },
  year: {
    required : "생년월일을 선택해주세요."
  },
  mouth: {
    required : "생년월일을 선택해주세요."
  },
  day: {
    required : "생년월일을 선택해주세요."
  },
  email : {
    required : '이메일을 입력하세요.',
    email : '올바른 이메일 주소를 입력해 주세요.'
  }
}

export function msgValidate(v, field, value = null,  valueMax = null) {
  for (let parameter in v.$params) {
    if (!v[parameter]) {
      return getMsgValidate[field][parameter] ?? '이 필드는 유효하지 않습니다\n'
    }
  }
}
