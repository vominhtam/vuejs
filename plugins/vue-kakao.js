import Vue from 'vue'
import VueKakaoSdk from 'vue-kakao-sdk'
import {KAKAO_SHARE} from "../config";

const apiKey = KAKAO_SHARE

// You have to pass your "Kakao SDK Javascript apiKey"
Vue.use(VueKakaoSdk, { apiKey })
